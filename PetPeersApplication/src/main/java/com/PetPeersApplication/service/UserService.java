package com.PetPeersApplication.service;

import java.util.List;
import java.util.Set;

import com.PetPeersApplication.exception.UserValidation;
import com.PetPeersApplication.model.Pet;
import com.PetPeersApplication.model.User;

public interface UserService {

	public abstract User addUser(User user) throws UserValidation;

	public abstract User updateUser(User user);

	public abstract List<User> listUsers();

	public abstract User getUserById(long userId);

	public abstract User findByUserName(String userName) throws UserValidation;

	public abstract long removeUser(long userId);

	public abstract Pet buyPet(long petId);

	public abstract Set<Pet> getmyPets();//getMyPets

	

}
