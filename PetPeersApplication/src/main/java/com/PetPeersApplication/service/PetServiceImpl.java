package com.PetPeersApplication.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.PetPeersApplication.dao.PetDao;
import com.PetPeersApplication.exception.UserValidation;
import com.PetPeersApplication.model.Pet;
import com.PetPeersApplication.model.User;

@Service

public class PetServiceImpl implements PetService {

	@Autowired
	private PetDao petDao;

	@Override
	public Pet savePet(Pet pet) throws UserValidation {
		Pet addPet = null;
		if (pet.getPetName() != "") {

			if (pet.getPetPlace() != "") {

				if (pet.getPetAge() > 0 && pet.getPetAge() <= 99) {

					addPet = petDao.save(pet);

				} else {
					throw new UserValidation("PetAge should be between 0 and 99 years!!");

				}

			} else {
				throw new UserValidation("PetPlace should not be empty!!");

			}

		} else {
			throw new UserValidation("PetName should not be empty!!");

		}

		return addPet;
		
	}

	@Override
	public List<Pet> getAllPets() {
		List<Pet> pets = petDao.findAll();
		return pets;
	}

	@Override
	public Pet getPetsById(long petId) {
//validate
		return petDao.getById(petId);
	}

	@Override
	public Pet updatePet(Pet pet) {
		
		return petDao.save(pet);
	}

	@Override
	public Pet getpetStatus(long petId) {
		List<Pet> pets = getAllPets();
		Pet resultPet=null;
		for (Pet pet : pets) {
			if(pet.getId()==petId) {
				if(pet.getOwner()==null) {
					resultPet=pet;
				}else {
					
				}
			}
		}
		
		return resultPet;
	}

}
