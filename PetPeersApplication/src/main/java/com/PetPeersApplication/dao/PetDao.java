package com.PetPeersApplication.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.PetPeersApplication.model.Pet;

public interface PetDao extends JpaRepository<Pet, Long>{

}
