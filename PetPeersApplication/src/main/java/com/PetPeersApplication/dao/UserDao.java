package com.PetPeersApplication.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.PetPeersApplication.model.Pet;
import com.PetPeersApplication.model.User;

public interface UserDao extends JpaRepository<User, Long> {

	public abstract User findByUserName(String username);
	
	
	
}
