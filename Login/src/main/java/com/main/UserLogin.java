package com.main;

import java.util.Scanner;

import com.dao.UserDao;
import com.dao.UserDaoImpl;
import com.exception.NumberException;
import com.model.User;
import com.service.UserService;
import com.service.UserServiceImpl;

public class UserLogin {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter UserId : ");
		int userId = scanner.nextInt();
		System.out.println("Enter Password : ");
		String password = scanner.next();

		UserService userService = new UserServiceImpl();
		User user;
		try {
			user = userService.checkCredentails(userId, password);
			if (user != null) {
				System.out.println("Welcome! " + user.getUserName());
			} else {
				System.out.println("Invalid User");
			}

		} catch (NumberException e) {
		System.err.println(e.getMessage());
		}

	}
}
