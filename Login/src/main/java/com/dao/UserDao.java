package com.dao;

import com.model.User;

public interface UserDao {
	public abstract User checkLogin(int userId, String password);
}
