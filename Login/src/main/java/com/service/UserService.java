package com.service;

import com.exception.NumberException;
import com.model.User;

public interface UserService {
	public abstract User checkCredentails(int userId, String password) throws NumberException;

}
